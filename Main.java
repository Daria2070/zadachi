package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите первое число (x): ");
        double x = in.nextDouble();

        System.out.println("Введите второе число (y): ");
        double y = in.nextDouble();
        System.out.println("Введите арифметический знак (begin): ");
        char begin = in.next().charAt(0);
        switch (begin) {
            case '+': System.out.println("Результат сложения:  "+ (x+y));
                break;
            case '-': System.out.println("Результат вычитания:  "+ (x-y));
                break;
            case '*': System.out.println("Результат умножения:  "+ (x*y));
                break;
            case '/': System.out.println("Результат деления:  "+ (x/y));
                break;
            default: System.out.println("Ошибка ввода данных");
                break;
        }

            }
}
